<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order as Orders;
use App\Models\Product as Products;

class OrdersController extends Controller
{

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {
        // your logic goes here.

        $arr_order_details = [];
        $search_order = Orders::find($id);
        $arr_order_details['order_id'] = $search_order->orderNumber;
        $arr_order_details['order_date'] = $search_order->orderDate;
        $arr_order_details['status'] = $search_order->status;
        $orderDetails = Orders::find($id)->orderdetails;
        $prod_details_container = [];
        $getNetPrice = 0;
        $i = 0;
        foreach($orderDetails as $order_detail)
        {
            //echo "<pre>";print_r($msg);echo "</pre>";
            $product_code = $order_detail->productCode;
            $unit_price = $order_detail->priceEach;
            $pquantity = $order_detail->quantityOrdered;
            $prod_details = Products::find($product_code);
            
            $prod_details['unit_price'] = $unit_price;
            $prod_details['qty'] = $pquantity;
            $total_price = number_format((float)$pquantity * $unit_price, 2, '.', '');
            $getNetPrice += $total_price;

            $prod_details['line_total'] = $total_price;

            $prod_details_container[$i] = $prod_details;
            //echo "<pre>";print_r($prod_details);echo "</pre>";
            //echo response()->json($prod_details, 200);
            $i++;
        }
        $arr_order_details['order_details'] = $prod_details_container;
        $arr_order_details['bill_amount'] = $getNetPrice;
        $customer_details = Orders::find($id)->customer;
        $arr_order_details['customer'] = $customer_details;
        //echo "<pre>";print_r($message);echo "</pre>";
        
        return response()->json($arr_order_details, 200);
    }
}
